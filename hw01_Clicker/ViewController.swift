//
//  ViewController.swift
//  hw01_Clicker
//
//  Created by Adam Grygar on 27/02/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var clicks: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        numberLabel.text = "\(clicks)"
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBAction func clickButton(_ sender: Any) {

        clicks += 1
        if clicks == 2 {
            textLabel.text = "Clicks"
        }
        if clicks == 1000 {
            numberLabel.font = numberLabel.font.withSize(120)
        }
        numberLabel .text = "\(clicks)"
    }
    
}

